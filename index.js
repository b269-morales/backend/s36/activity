// Setup dependencies

const express = require("express");
const mongoose = require("mongoose");

// "Require" from different directories

const taskRoute = require("./routes/taskRoute.js");

// Server Setup

const app = express();
const port = 3001;

// Middlewares

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database Connection

mongoose.connect("mongodb+srv://ajbmrls:admin123@zuitt-bootcamp.mbim4pe.mongodb.net/s36?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

mongoose.connection.once("open", () => console.log(`Now connected to the cloud database.`));

// Add task route
// Allows all the task routes created in the "taskRoute.js" file to use "/tasks" route
app.use("/tasks", taskRoute);




app.listen(port, () => console.log(`Now listening to ${port}.`));