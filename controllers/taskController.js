// Contains instructions on HOW your API will perform its intended tasks
// ALL the OPERATIONS it can do will be placed in this file

const Task = require("../models/task.js");

// Controller function for getting all the tasks
// Note: this is a function that still has NO INVOCATION- it will be invoked in 'taskRoute.js'
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
};


// Controller function for creating a task

module.exports.createTask = (requestBody) => {
  return Task.findOne({ name: requestBody.name }).then((task) => {
    if (task != null && task.name == requestBody.name) {
      return "Duplicate task found";
    } 
    else {
      let newTask = new Task({
        name: requestBody.name,
      });

      return newTask.save().then((savedTask, saveErr) => {
        if (saveErr) {
          console.log(saveErr);
          return false;
        } else {
          return savedTask;
        }
      });
    }
  });
};



// Controller function for deleting a task
/*
Business Logic
1. Look for the task with the corresponding id provided in the URL/route
2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/
module.exports.deleteTask = (taskID) => {
	return Task.findByIdAndRemove(taskID).then((removedTask, err) => {
		if (err){
			console.log(err);
			return false;
		} else {
			return "Deleted Task."
		}
	})
};


/*==================================================================*/
/*=========================ACTIVITY=================================*/
/*==================================================================*/


// Controller function for getting a specific task
module.exports.getTask = (taskID) => {
	return Task.findById(taskID).then(result => {
		return result;
	})
};


// Controller function for changing 'status' of task 
	module.exports.updateTaskStatusComplete = (taskID) => {
		return Task.findById(taskID).then(result => {
			if (result.status == "complete") {
				result.status = "pending";
				return result.save().then((savedTask, saveErr) => {
					if (saveErr) {
						console.log(saveErr);
						return false;
					} else {
						return savedTask;
					} 
				});
			} else {
				result.status = "complete";
				return result.save().then((savedTask, saveErr) => {
					if (saveErr) {
						console.log(saveErr);
						return false;
					} else {
						return savedTask;
					}
				});
			}
		});
	};

