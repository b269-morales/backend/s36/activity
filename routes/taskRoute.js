// Defines WHEN particular controllers will be used
// Contains all the endpoints and responses that we can get from controllers


const express = require("express");

// Creates a router instance that functions as a middleware and routing system
const router = express.Router();

const taskController = require("../controllers/taskController.js");

// Route to get all the tasks
router.get("/", (req,res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// Route to create task
router.post("/", (req,res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

// Route to delete task
router.delete("/:id", (req,res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
});




/*==================================================================*/
/*=========================ACTIVITY=================================*/
/*==================================================================*/


// Route to get specific task
router.get("/:id", (req,res) => {
	taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

// Route to put 'status' of a specific task
router.put("/:id", (req,res) => {
	taskController.updateTaskStatusComplete(req.params.id).then(resultFromController => res.send(resultFromController));
});




module.exports = router;